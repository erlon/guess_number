#ifndef GUESS_NUMBER_RANDOM_H
#define GUESS_NUMBER_RANDOM_H

#include <stdlib.h>
#include <stdint.h>

void random_setup_seed(void);
int random_generate(uint8_t min, uint8_t max);

#endif //GUESS_NUMBER_RANDOM_H
