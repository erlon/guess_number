#ifndef GUESS_NUMBER_GAME_H
#define GUESS_NUMBER_GAME_H

#include <stdlib.h>

typedef enum game_state_t {
    GAME_STATE_GAME_OVER,
    GAME_STATE_PLAYING,
    GAME_STATE_PLAYER_WON
} game_state_t;

typedef enum difficulty_t {
    DIFFICULTY_EASY = 16
} difficulty_t;

void game_setup(difficulty_t game_difficulty);

game_state_t game_current_state(void);

void game_guess_number(int number);

void game_draw(void);

#endif //GUESS_NUMBER_GAME_H
