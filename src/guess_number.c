#include <stdio.h>

#include "digi.h"
#include "game.h"

int main(void) {
    game_setup(DIFFICULTY_EASY);
    game_draw();
    int number = 0;

    while (game_current_state() == GAME_STATE_PLAYING) {
        do {
            printf("Number to guess: ");
        } while (!read_int(&number));

        game_guess_number(number);
        game_draw();
    }
}
