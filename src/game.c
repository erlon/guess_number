#include <stdio.h>

#include "game.h"
#include "random.h"

#define MAX_ATTEMPTS 6
#define LOWER_LIMIT 1

difficulty_t difficulty;
int attempts, last_guess, current_state, random_number;

void game_setup(difficulty_t game_difficulty) {
    random_setup_seed();

    attempts = 0;
    last_guess = 0;
    difficulty = game_difficulty;

    random_number = random_generate(LOWER_LIMIT, (uint8_t)difficulty);
    current_state = GAME_STATE_PLAYING;
}

game_state_t game_current_state(void) {
    return current_state;
}

void game_draw(void) {
    printf("Attempt: %d\n", attempts);

    if (current_state == GAME_STATE_PLAYING) {
        if (attempts == 0) {
            puts("Good luck");
        } else {
            if (last_guess < 0) {
                puts("Invalid number entered");
                return;
            }
            if (last_guess > random_number) {
                puts("Is too high!");
            }
            if (last_guess < random_number) {
                puts("Is too low!");
            }
        }
    }

    if (current_state == GAME_STATE_PLAYER_WON) {
        printf("You won!\nCorrect Number: %d\n", random_number);
    }

    if (current_state == GAME_STATE_GAME_OVER) {
        printf("You lost!\nCorrect Number: %d\n", random_number);
    }
}

void game_guess_number(int guessed_number) {
    last_guess = guessed_number;
    if (guessed_number == random_number) {
        current_state = GAME_STATE_PLAYER_WON;
        return;
    }

    ++attempts;
    if (attempts > MAX_ATTEMPTS) {
        current_state = GAME_STATE_GAME_OVER;
    }
}
