#include <time.h>

#include "random.h"

void random_setup_seed(void) {
    srand(time(NULL));
}

int random_generate(uint8_t min, uint8_t max) {
    return min + rand() % (max - min);
}
